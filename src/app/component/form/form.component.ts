import { Component, OnInit, state } from '@angular/core';
import { State } from '../../enum/state.enum';
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  state = State;
  stateLebele = Object.values(State);
  form: FormGroup;

  nom: string;
  ref: string;
  etat: string;

  newItem  = {
    id: '',
    nom: '',
    ref: '',
    state: ''
  };



  constructor( private fb: FormBuilder) { }

  ngOnInit() {
      this.createForm();
  }



  process() {
    console.log('newItem-------', this.form.value);
}
isErros(val: string): boolean {
  return this.form.get(val).invalid && this.form.get(val).touched;
}

isErrosVide(val: string): boolean {
  return this.form.get(val).invalid && this.form.get(val).touched;
}
isErrosLinght(val: string): boolean {
  return this.form.get(val).invalid && this.form.get(val).touched;
}

createForm() {
  this.form = this.fb.group({
    id: '',
    nom: [
      '',
      Validators.compose([Validators.required, Validators.minLength(5)])
    ],
    ref: [
      '',
      Validators.compose([Validators.required, Validators.minLength(5)])
    ],
    state: ''
  });
}
}
