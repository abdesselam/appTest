import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';


const appRoutes: Routes = [

  {
    path: 'home',
    component: HomeComponent},
  { path: '/home',
    redirectTo: '/heroes',
    pathMatch: 'full'
  },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactsComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
    // other imports here
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
